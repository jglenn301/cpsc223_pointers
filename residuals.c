#include <stdio.h>

/**
 * Reads numbers from standard input and outputs the difference between
 * them and their mean.
 */

double calculate_mean(int n, const double *arr);
void add_all(int n, double *arr, double x);
void print_array(int n, const double *arr);

double *read_array(FILE *in, int *n);

int main()
{
  int n;
  double *input = read_array(stdin, &n);
  
  double mean = calculate_mean(n, input);
  add_all(n, input, -mean);
  print_array(n, input);

  return 0;
}

void add_all(int n, double *arr, double x)
{
  for (int i = 0; i < n; i++)
    {
      arr[i] += x;
    }
}

double calculate_mean(int n, const double *arr)
{
  double sum = 0.0;
  for (int i = 0; i < n; i++)
    {
      sum += arr[i];
    }

  return sum / n;
}

void print_array(int n, const double *arr)
{
  printf("[");
  for (int i = 0; i < n; i++)
    {
      if (i > 0)
	{
	  printf(", ");
	}
      printf("%lf", arr[i]);
    }
  printf("]\n");
}

/**
 * Reads the number of inputs from from the given file,
 * creates an array of doubles of that size, reads from the file
 * into that array, and returns the result.  The integer pointed
 * to by the second parameter is set to the size of the array.
 *
 * @param in a pointer to a file
 * @param n a pointer to an int
 * @return a pointer to the new array, or NULL
 */
double *read_array(FILE *in, int *n)
{
  fscanf(in, "%d", n);

  if (*n > 0)
    {
      double input[*n]; // NO NO NO NO!  This is the root cause of the segfault!
      for (int c = 0; c < *n; c++)
	{
	  if (scanf("%lf", input + c) != 1)
	    {
	      // couldn't read next input; return what we were able to read
	      *n = c;
	      return input;
	    }
	}
      
      return input;
    }
  else
    {
      return NULL; // special value indicating the pointer points to nowhere
    }
}
