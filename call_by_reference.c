#include <stdio.h>

void increment(int *x);

int main()
{
  int a = 10;

  increment(&a);

  printf("a=%d\n", a); // 11!
}

// increments the int passed to it using (simulated) call-by-reference
void increment(int *x)
{
  *x = *x + 1;
}
