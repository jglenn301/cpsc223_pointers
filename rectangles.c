#include <stdio.h>

/**
 * A struct to represent a rectangle in a graphics context.
 * A rectangle is specified by the coordinates of one corner and
 * the dimensions.
 */
typedef struct _rectangle
{
  int left;
  int top;
  int width;
  int height;
} rectangle;


/**
 * Returns an enlarges copy of the given rectangle.
 *
 * @param r a pointer to a rectangle
 * @param scale a double
 * @return a copy of r with dimensions scaled by scale
 */
rectangle enlarged(const rectangle *r, double scale)
{
  rectangle result = {r->left, r->top, r->width * scale, r->height * scale};
  return result;
}

int main(int argc, char *argv[])
{
  //rectangle home = {20, 40, 15, 25};
  rectangle home;
  home.left = 20;
  home.top = 40;
  home.width = 15;
  home.height = 25;
  rectangle bigger = enlarged(&home, 2.0);

  printf("%d %d %d %d\n", home.left, home.top, home.width, home.height);
  printf("%d %d %d %d\n", bigger.left, bigger.top, bigger.width, bigger.height);
  
}


