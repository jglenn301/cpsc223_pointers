#include <stdio.h>
#include <stdlib.h>

long factorial(int n);

int main(int argc, char *argv[])
{
  if (argc < 2)
    {
      fprintf(stderr, "USAGE: %s n\n", argv[0]);
      return 1;
    }

  int n = atoi(argv[1]);

  if (n < 0 || n > 20)
    {
      fprintf(stderr, "%s: n must be nonnnegative and no greater than 20\n");
      return 1;
    }

  long result = factorial(n);
  printf("%d! = %ld\n", n, result);
}

long factorial(int n)
{
  if (n == 0)
    {
      return 1;
    }
  else
    {
      long smaller = factorial(n - 1);
      return n * smaller;
    }
}
